# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import models
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from dataloader import GDataset, TDataset
from utils import tensor_normalizer
import numpy as np
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# Hyper Parameters
batch_size = 32

mode = "test"

def eval_train_data(net, transform_method):
    dataset = GDataset("/data/yushang/dataset/train/", "train.csv", transform=transform_method, filter_mode=0)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)

    result = []
    embed_list = []
    part = 0
    for i, (images, labels, ids) in enumerate(dataloader):
        images = Variable(images).to(device)
        labels = Variable(labels)#.to(device)
        logit, embed = net(images)
        for k in range(len(labels)):
            truth = labels.numpy()[k]
            id = ids[k]
            k_th_vec = logit[k,:].data.cpu().numpy()
            pred = np.argmax(k_th_vec)
            confidence = k_th_vec[pred]
            result.append([id,pred,confidence,truth])
            print(str(len(result)) + ": " + str(result[-1]))
            embed_k = embed[k,:].data.cpu().numpy()
            embed_list.append([id, embed_k])
            if len(embed_list) >= 200:
                eval("np.savez_compressed('output/imagenet_embed/' + str(part),"
                    + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
                                for j, (id, embed_k) in enumerate(embed_list)]) + ')')
                part += 1
                embed_list = []
    eval("np.savez_compressed('output/imagenet_embed/' + str(part),"
         + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
                     for j, (id, embed_k) in enumerate(embed_list)]) + ')')
    save_newid(result)


def eval_test_data(net, transform_method):
    dataset = TDataset("/data/yushang/dataset/test/", "test.csv", transform=transform_method)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)
    result = []
    embed_list = []
    part = 0
    for i, (images, labels, ids) in enumerate(dataloader):
        images = Variable(images).to(device)
        labels = Variable(labels)#.to(device)
        logit, embed = net(images)
        for k in range(len(labels)):
            truth = labels.numpy()[k]
            id = ids[k]
            k_th_vec = logit[k,:].data.cpu().numpy()
            pred = np.argmax(k_th_vec)
            confidence = k_th_vec[pred]
            result.append([id,pred,confidence])
            print(str(len(result)) + ": " + str(result[-1]))
            embed_k = embed[k,:].data.cpu().numpy()
            embed_list.append([id, embed_k])
            if len(embed_list) >= 200:
                eval("np.savez_compressed('output/imagenet_embed_test/' + str(part),"
                    + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
                                for j, (id, embed_k) in enumerate(embed_list)]) + ')')
                part += 1
                embed_list = []
    eval("np.savez_compressed('output/imagenet_embed_test/' + str(part),"
         + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
                     for j, (id, embed_k) in enumerate(embed_list)]) + ')')


def main():
    net = Resnet().to(device)
    net.eval()
    print('init net')
    transform_method = transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor(),
        tensor_normalizer
    ])

    if mode == "train":
        eval_train_data(net, transform_method)
    elif mode == "test":
        eval_test_data(net, transform_method)



def save_newid(result):
    with open("output/imagenet_id.csv", 'w') as f:
        result_desc = sorted(result, key=lambda x: x[2], reverse=True)
        for id, pred, confidence, truth in result_desc:
            f.write(id + ',' + str(pred) + '-' + str(truth) + ',' + str(confidence) + '\n')

class Resnet(nn.Module):
    def __init__(self):
        super(Resnet, self).__init__()
        self.resnet = models.resnet152(pretrained=True)

    def forward(self, input):
        for i, (name, module) in enumerate(self.resnet._modules.items()):
            if name=='fc':
                break
            input = module(input)
        embed = input.view(input.size(0), -1)
        logit = self.resnet.fc(embed)
        return logit, embed


if __name__ == '__main__':
    main()
