# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import dataloader
from networks import Resnet
from collections import OrderedDict
import os
# Hyper Parameters
num_epochs = 200
batch_size = 1400
learning_rate = 0.001
is_new_model = False
aug_mode = 0
model_path = 'model/model_' + str(aug_mode) + '.pkl'
data_path = "/data/yushang/dataset/train/"
num_class = 203094
device_ids = [0, 1, 2, 3, 4, 5, 6, 7]

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
def main():
    net0 = Resnet(num_class=num_class, use_pretrain=is_new_model)
    #net = net0
    net = torch.nn.DataParallel(net0, device_ids=device_ids).to(device)
    #net = parallel.DataParallelModel(net0, device_ids=device_ids).to(device)
    if is_new_model:
        global_step = 0
        print('train from new model')
    else:
        ckpt = net0.load(model_path)
        global_step = ckpt['global_step']
        dist_state_dict = OrderedDict()
        for k, v in ckpt['state_dict'].items():
            name = k.replace('module.', '')  # remove the 'module' prefix.
            dist_state_dict[name] = v
        net.module.load_state_dict(dist_state_dict)
        print('train from last model')

    net.train()
    print('init net')
    #criterion = nn.CrossEntropyLoss()
    #criterion = parallel.DataParallelCriterion(criterion, device_ids)
    optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate, weight_decay=0.000000)
    #optimizer = torch.optim.SGD([{'params':net.parameters(), 'initial_lr': learning_rate}], lr=learning_rate, weight_decay=0.000000)
    #scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.9, last_epoch=int(global_step / 4132914.0))
    try:
        optimizer.load_state_dict(ckpt['optim_dict'])
        print('load optim_dict')
    except:
        pass

    # Train the Model
    train_dataloader = dataloader.get_train_data_loader(path=data_path,metafile="train.csv",num_class=num_class,
                                                        batch_size=batch_size,aug_mode=aug_mode)
    ts = time.time()
    for epoch in range(num_epochs):
        for i, (images, labels, id) in enumerate(train_dataloader):
            images = Variable(images).to(device)
            labels = Variable(labels.long()).to(device)
            #logit, embed = net(images)
            logit, embed, loss = net(images, labels)
            #logit, embed = zip(*net(images))
            #logit = [(e,) for e in logit]
            #loss = criterion(logit, labels)
            loss = loss.mean()
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            global_step += 1
            if (i + 1) % 10 == 0:
                speed = '%.2f' % (len(labels) * 10.0 / (time.time() - ts))
                ts = time.time()
                print("epoch:", epoch, "step:", i, 'global_step:', global_step, 'samples/sec:', speed, "loss:",
                      loss.item())
            if (i + 1) % 200 == 0:
                net0.save(global_step, net.state_dict(), optimizer.state_dict(), model_path)  # current is model.pkl
                print("save model")
        print("epoch:", epoch, "step:", i, 'global_step:', global_step, "loss:", loss.item())
        net0.save(global_step, net.state_dict(), optimizer.state_dict(), model_path)  # current is model.pkl
        print("save last model")











if __name__ == '__main__':
    main()
