# -*- coding: UTF-8 -*-
import os
import torch
from torch.utils.data import DataLoader,Dataset
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as dset
from PIL import Image
from utils import *
#device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class GDataset(Dataset):
    def __init__(self, folder, metafile, transform=None, num_class=0, filter_mode=0):
        with open(os.path.join(folder, metafile)) as f:
            self.meta_data = [(id, lid) for id, url, lid in [line.split(',') for line in f.readlines()[1:]]]
        if filter_mode == 1:
            self.meta_data = [e for e in self.meta_data if e[0][-1] != '7']
        elif filter_mode == 2:
            self.meta_data = [e for e in self.meta_data if e[0][-1] == '7']
        def id2path(id):
            return folder + '/'.join(id[:3]) + '/' + id + '.jpg'
        self.train_image_file_paths = [id2path(id) for id, lid in self.meta_data]
        self.transform = transform
        self.num_class = num_class

    def __len__(self):
        return len(self.train_image_file_paths)

    def __getitem__(self, idx):
        image_path = self.train_image_file_paths[idx]
        image = Image.open(image_path)
        if self.transform is not None:
            image = self.transform(image)
        id = self.meta_data[idx][0]
        lid = self.meta_data[idx][1].strip()
        # label = np.zeros(self.num_class + 1, dtype=float)
        # label[lid] = 1.0
        label = int(lid)
        return image, label, id

transform_method_list_train = [
    trans_generator(keep_aspect_ratio=False, random_crop=True),
    trans_generator(keep_aspect_ratio=False, rotate=True, random_crop=True),
    trans_generator(keep_aspect_ratio=False, color=True, random_crop=True),
    trans_generator(keep_aspect_ratio=False, rotate = True, color=True, random_crop=True),
    trans_generator(keep_aspect_ratio=True, random_crop=True),
    trans_generator(keep_aspect_ratio=True, rotate=True, random_crop=True),
    trans_generator(keep_aspect_ratio=True, color=True, random_crop=True),
    trans_generator(keep_aspect_ratio=True, rotate=True, color=True, random_crop=True),
]

transform_method_list_test = [
    trans_generator(keep_aspect_ratio=False, random_crop=False),
    trans_generator(keep_aspect_ratio=False, rotate=True, random_crop=False),
    trans_generator(keep_aspect_ratio=False, color=True, random_crop=False),
    trans_generator(keep_aspect_ratio=False, rotate = True, color=True, random_crop=False),
    trans_generator(keep_aspect_ratio=True, random_crop=False),
    trans_generator(keep_aspect_ratio=True, rotate=True, random_crop=False),
    trans_generator(keep_aspect_ratio=True, color=True, random_crop=False),
    trans_generator(keep_aspect_ratio=True, rotate=True, color=True, random_crop=False),
]

trans_train = transforms.Compose([
    transforms.Lambda(lambda img: randomResize(img, 228, 320)),
    transforms.RandomCrop(224),
    transforms.RandomHorizontalFlip(),
    transforms.RandomApply([transforms.ColorJitter(brightness=0.4,contrast=0.4,saturation=0.4,hue=0)],p=0.2),
    transforms.RandomApply([transforms.RandomRotation(20)],p=0.05),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

trans_test = transforms.Compose([
    transforms.Lambda(lambda img: randomResize(img, 228, 228)),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

def get_train_data_loader(path, metafile, num_class, batch_size = 8, aug_mode = 0):
    #transform_method = transform_method_list_train[aug_mode]
    dataset = GDataset(path, metafile, transform=trans_train, num_class=num_class, filter_mode=0)
    return DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=24, pin_memory=True, drop_last=True)


def get_test_data_loader(path, metafile, num_class, batch_size = 1, aug_mode = 0):
    transform_method = transform_method_list_test[aug_mode]
    dataset = GDataset(path, metafile, transform=transform_method, num_class=num_class, filter_mode=2)
    return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=24)

def get_dist_train_dataset(path, metafile, num_class, batch_size = 8, aug_mode = 0):
    transform_method = transform_method_list_train[aug_mode]
    dataset = GDataset(path, metafile, transform=transform_method, num_class=num_class, filter_mode=1)
    return dataset



def get_largest_id(metapath):
    with open(metapath) as f:
        meta_data = [(id, lid) for id, url, lid in [line.split(',') for line in f.readlines()[1:]]]
    lidset = set()
    lidmax = 0
    lidmin = 100
    cnt = {}
    for id, lid in meta_data:
        lidset.add(lid)
        lidmax = max(lidmax, int(lid))
        lidmin = min(lidmin, int(lid))
        if lid not in cnt:
            cnt[lid]=0
        cnt[lid] += 1
    print(len(meta_data))
    print(lidmin)
    print(lidmax)
    print(len(lidset))
    bin = [0,5,10,50,100,1e3,1e5]
    max_cat = 0
    min_cat = 100
    cat_num_dist = [0] * 6
    pic_num_dist = [0] * 6
    for lid, num in cnt.items():
        max_cat = max(max_cat, cnt[lid])
        min_cat = min(min_cat, cnt[lid])
        for b in range(len(bin)-1):
            if cnt[lid]>bin[b] and cnt[lid]<=bin[b+1]:
                cat_num_dist[b]+=1
                pic_num_dist[b]+=cnt[lid]
                break
    print(cat_num_dist)
    print(pic_num_dist)
    print(min_cat)
    print(max_cat)


# class TDataset(Dataset):
#     def __init__(self, folder, metafile, transform=None, num_class=0):
#         # with open(os.path.join(folder, metafile)) as f:
#         #     self.meta_data = [id.strip('"') for id, url in [line.split(',') for line in f.readlines()[1:]]]
#         # def id2path(id):
#         #     return folder + 'test1/' + id + '.jpg'
#         # self.train_image_file_paths = [id2path(id) for id in self.meta_data]
#         # self.train_image_file_paths = [path for path in self.train_image_file_paths if os.path.exists(path)]
#         self.train_image_file_paths = [folder + 'test1/' + name for name in os.listdir(os.path.join(folder, 'test1'))]
#         self.meta_data = [path.split('/')[-1].split('.')[0] for path in self.train_image_file_paths]
#         self.image_dict = {}
#         for i, id in enumerate(self.meta_data):
#             self.image_dict[id] = self.train_image_file_paths[i]
#         self.train_image_file_paths = [path for id, path in self.image_dict.items()]
#         self.meta_data = [path.split('/')[-1].split('.')[0] for path in self.train_image_file_paths]
#         self.transform = transform
#         self.num_class = num_class
#
#     def __len__(self):
#         return len(self.train_image_file_paths)
#
#     def __getitem__(self, idx):
#         image_path = self.train_image_file_paths[idx]
#         image = Image.open(image_path)
#         if (image.mode != 'RGB'):
#             image = image.convert("RGB")
#         if self.transform is not None:
#             image = self.transform(image)
#         id = self.meta_data[idx]
#         label = int(0)
#         return image, label, id


class T2Dataset(Dataset):
    def __init__(self, folder, metafile, transform=None, num_class=0):
        with open(os.path.join(folder, metafile)) as f:
            self.meta_data = [id.strip() for id in f.readlines()[1:]]
        def id2path(id):
            return folder + '/'.join(id[:3]) + '/' + id + '.jpg'
        self.train_image_file_paths = [id2path(id) for id in self.meta_data]
        self.transform = transform
        self.num_class = num_class

    def __len__(self):
        return len(self.train_image_file_paths)

    def __getitem__(self, idx):
        image_path = self.train_image_file_paths[idx]
        image = Image.open(image_path)
        if (image.mode != 'RGB'):
            image = image.convert("RGB")
        if self.transform is not None:
            image = self.transform(image)
        id = self.meta_data[idx]
        label = int(0)
        return image, label, id


# def get_test1_data_loader(path, metafile, num_class, batch_size = 8, aug_mode = 0):
#     #transform_method = transform_method_list_test[aug_mode]
#     dataset = TDataset(path, metafile, transform=trans_test, num_class=num_class)
#     return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8, pin_memory=True)

def get_stage2_data_loader(path, metafile, num_class, batch_size=8):
    dataset = T2Dataset(path, metafile, transform=trans_test, num_class=num_class)
    return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8, pin_memory=True)

if __name__ == '__main__':
    get_largest_id('../train.csv')
