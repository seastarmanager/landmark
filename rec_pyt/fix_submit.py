
with open('../pair_result_bal.txt') as f:
    raw = f.readlines()[1:]

id2line = {}
for line in raw:
    id = line.strip().split(',')[0]
    id2line[id] = line

lid_dict = {}
for line in raw:
    id = line.strip().split(',')[0]
    lid = line.strip().split(' ')[0].split(',')[1]
    c = float(line.strip().split(' ')[1])
    if lid not in lid_dict:
        lid_dict[lid] = []
    lid_dict[lid].append([id, c])
lid_max_list = []
for lid, v in lid_dict.items():
    max_id, max_c = sorted(v, key=lambda x:x[1], reverse=True)[0]
    lid_max_list.append([max_id, lid, max_c])
lid_max_list = sorted(lid_max_list, key=lambda x:x[2], reverse=True)
score = 0.8
for id, lid, c in lid_max_list:
    id2line[id] = id + ',' + lid + ' ' + str(score) + '\n'
    score -= 0.000001


with open('../test.csv') as f:
    all_id = [id.strip('"') for id, url in [line.split(',') for line in f.readlines()[1:]]]

print(len(all_id))

with open('../submit-pair_result_bal.csv', 'w') as f:
    f.write('id,landmarks\n')
    for id in all_id:
        if id not in id2line:
            f.write(id + ',\n')
        else:
            line = id2line[id]
            if len(line.split(' '))==2:
                first, c = line.strip().split(' ')
                c = float(c)
                #c = 0.2
                _, cat = first.split(',')
                # if cat == '123113':
                #     c = c * 0.5
                #f.write(id2line[id])
                f.write(id + ',' + cat + ' ' + str(c) + '\n')
            else:
                p1,p2,p3 = line.split(',')
                f.write(p1+','+p2+' '+p3)
