# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import dataloader
from networks import Resnet
import os
import torch.distributed as dist
import torch.multiprocessing as mp
from utils import partition_dataset
# Hyper Parameters
num_epochs = 1
batch_size = 32
learning_rate = 0.001
is_new_model = True
aug_mode = 0
model_path = 'model/model_dist_' + str(aug_mode) + '.pkl'
data_path = "/data/yushang/dataset/train/"
num_class = 203094
gpu_ids = [1,2,3]

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def main():
    world_size = len(gpu_ids)

    # 汇总多卡的梯度，平均后传播
    def average_gradients(model):
        size = float(dist.get_world_size())
        for p in model.parameters():
            if p.grad is not None:
                dist.all_reduce(p.grad.data, op=dist.ReduceOp.SUM)
                p.grad.data /= size

    def train(epoch, gpu_id, model, optimizer, data_loader):
        model.train()
        train_set, bsz = partition_dataset()
        ts = time.time()
        for i, data in enumerate(data_loader):
            data = {key: value.to(gpu_id) for key, value, ids in data.items()}
            model.zero_grad()
            loss = model.forward(data)
            loss.backward()
            if world_size > 1:
                average_gradients(model)
            optimizer.step()
            if gpu_id == gpu_ids[0]:
                if (i + 1) % 10 == 0:
                    speed = '%.2f' % (bsz * 10.0 / (time.time() - ts))
                    ts = time.time()
                    print("epoch:", epoch, "step:", i, 'samples/sec:', speed, "loss:",
                          loss.item())
                if (i + 1) % 200 == 0:
                    model.save(0, model.state_dict(), optimizer.state_dict(), model_path)  # current is model.pkl
                    print("save model")

        if gpu_id == gpu_ids[0]:
            print("epoch:", epoch, "step:", i, "loss:", loss.item())
            model.save(0, model.state_dict(), optimizer.state_dict(), model_path)  # current is model.pkl
            print("save last model")

    def run(gpu_id):
        model = Resnet(num_class=num_class, use_pretrain=is_new_model).to(device)
        if is_new_model:
            global_step = 0
            print('train from new model')
        else:
            ckpt = model.load(model_path)
            global_step = ckpt['global_step']
            model.load_state_dict(ckpt['state_dict'])
            print('train from last model')
        model.train()
        print('init net')
        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=0.000000)
        try:
            optimizer.load_state_dict(ckpt['optim_dict'])
            print('load optim_dict')
        except:
            pass
        model.cuda(gpu_id)
        dataset = dataloader.get_dist_train_dataset(path=data_path,metafile="train.csv",num_class=num_class,
                                                    batch_size=batch_size,aug_mode=aug_mode)
        train_dataloader = partition_dataset(dataset, batch_size, shuffle=True)
        for epoch in range(num_epochs):
            train(epoch, gpu_id, model, optimizer, train_dataloader)


    def init_process(host, port, rank, fn, backend="nccl"):
        """
        host: str, 如果一机多卡可以直接填localhost
        port: str
        rank: int
        fn: train的函数
        backend: 实现多卡通信的机制，有多种，PyTorch有汇总
        """
        os.environ["MASTER_ADDR"] = host
        os.environ["MASTER_ADDR"] = port
        dist.init_process_group(backend, rank=rank, world_size=world_size)
        fn(rank)

    # 开始执行
    mp.set_start_method("spawn")

    processes = []
    # 有几张卡可以指定ranks列表
    for rank in gpu_ids:
        p = mp.Process(target=init_process, args=('localhost', 60000+int(rank), rank, run))
        p.start()
        processes.append(p)

        for p in processes:
            p.join()















if __name__ == '__main__':
    main()
