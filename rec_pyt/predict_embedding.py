# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
from dataloader import GDataset, T2Dataset, trans_train, trans_test
from torch.utils.data import DataLoader
from networks import Resnet, Resnet_embed
import numpy as np
from collections import OrderedDict
import torchvision.transforms as transforms
from utils import *
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# Hyper Parameters
mode = "test_multicrop"
batch_size = 64
aug_mode = 0
model_path = 'model/model_' + str(aug_mode) + '.pkl'
data_path = "/data/yushang/dataset/test/"
output_path = 'output/pred_result.txt'
num_class = 203094
device_ids = [0,]

def softmax(vec):
    tmp = np.max(vec)
    x = vec - tmp
    x = np.exp(x)
    tmp = np.sum(x)
    x /= tmp
    return x


def save(result):
    with open(output_path, 'w') as f:
        result_desc = sorted(result, key=lambda x: x[2], reverse=True)
        for id, pred, confidence in result_desc:
            f.write(id + ',' + str(pred) + ' ' + str(confidence) + '\n')


def eval_train_data(net, transform_method):
    dataset = GDataset("/data/yushang/dataset/train/", "train.csv", transform=transform_method, filter_mode=0)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)
    embed_list = []
    embed_dict = {}
    part = 0
    for i, (images, labels, ids) in enumerate(dataloader):
        images = Variable(images).to(device)
        #labels = Variable(labels).to(device)
        embed= net(images)
        for k in range(len(labels)):
            id = ids[k]
            print(i, id)
            embed_k = embed[k,:].data.cpu().numpy()
            embed_dict[id] = embed_k
    torch.save(embed_dict, 'output/softmax_embed_train.pkl')

def eval_test_data(net, transform_method):
    dataset = T2Dataset("/data/yushang/dataset/test/test2/", "test2.csv", transform=transform_method)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)
    result = []
    embed_list = []
    embed_dict = {}
    part = 0
    for i, (images, labels, ids) in enumerate(dataloader):
        images = Variable(images).to(device)
        labels = Variable(labels).to(device)
        logit, embed, loss = net(images, labels)
        for k in range(len(labels)):
            id = ids[k]
            print(i, id)
            k_th_vec = logit[k, :].data.cpu().numpy()
            k_th_soft = softmax(k_th_vec)
            pred = np.argmax(k_th_vec)
            confidence = k_th_soft[pred]
            result.append([id, pred, confidence])
            print(result[-1])
            embed_k = embed[k, :].data.cpu().numpy()
            embed_dict[id] = embed_k
    torch.save(embed_dict, 'output/softmax_embed_test.pkl')
    save(result)
    return result

def eval_test_multi_crop(net, transform_method):
    trans_multi = transforms.Compose([
        transforms.Lambda(lambda img: randomResize(img, 320, 320)),
        transforms.RandomCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
    dataset = T2Dataset("/data/yushang/dataset/test/test2/", "test2.csv", transform=transform_method)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=4)
    dataloader_list = [dataloader,]
    for j in range(5):
        dataset = T2Dataset("/data/yushang/dataset/test/test2/", "test2.csv", transform=trans_multi)
        dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=4)
        dataloader_list.append(dataloader)
    logit_dict = {}
    result = []
    for dataloader in dataloader_list:
        for i, (images, labels, ids) in enumerate(dataloader):
            images = Variable(images).to(device)
            labels = Variable(labels).to(device)
            logit, embed, loss = net(images, labels)
            print(i)
            for k in range(len(labels)):
                id = ids[k]
                k_th_vec = logit[k, :].data.cpu().numpy()
                if id not in logit_dict:
                    logit_dict[id] = k_th_vec
                else:
                    logit_dict[id] = logit_dict[id] + k_th_vec
    for id, k_th_vec in logit_dict.items():
        k_th_vec = k_th_vec / float(len(dataloader_list))
        k_th_soft = softmax(k_th_vec)
        pred = np.argmax(k_th_vec)
        confidence = k_th_soft[pred]
        result.append([id, pred, confidence])
        print(result[-1])
    save(result)




def main():
    if mode == "train":
        net0 = Resnet_embed(num_class=num_class, use_pretrain=False).to(device)
    else:
        net0 = Resnet(num_class=num_class, use_pretrain=False).to(device)
    net = torch.nn.DataParallel(net0, device_ids=device_ids).to(device)
    ckpt = net0.load(model_path)
    dist_state_dict = OrderedDict()
    for k, v in ckpt['state_dict'].items():
        name = k.replace('module.', '')  # remove the 'module' prefix.
        if mode=="train" and (name=="fc.1.weight" or name=="fc.1.bias"):
            continue
        dist_state_dict[name] = v
    net.module.load_state_dict(dist_state_dict)
    print('load from last model')
    net.eval()
    print('init net')

    if mode == "train":
        eval_train_data(net, trans_test)
    elif mode == "test":
        eval_test_data(net, trans_test)
    elif mode == "test_multicrop":
        eval_test_multi_crop(net, trans_test)










if __name__ == '__main__':
    with torch.no_grad():
        main()
