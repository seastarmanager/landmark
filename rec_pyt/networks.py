# -*- coding: UTF-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models

class Resnet(nn.Module):
    def __init__(self, num_class, use_pretrain=False):
        super(Resnet, self).__init__()
        self.resnet = models.resnet50(pretrained=True) if use_pretrain else models.resnet50()
        self.adavg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Sequential(
            nn.Linear(self.resnet.fc.in_features, 512),
            nn.Linear(512, num_class)
        )
    def forward(self, input, labels):
        criterion = nn.CrossEntropyLoss()
        for i, (name, module) in enumerate(self.resnet._modules.items()):
            if name=='avgpool':
                break
            input = module(input)
        input = self.adavg_pool(input)
        embed = input.view(input.size(0), -1)
        embed = self.fc[0](embed)
        logit = self.fc[1](embed)
        loss = criterion(logit, labels)
        loss = torch.unsqueeze(loss, 0)
        return logit, embed, loss

    @staticmethod
    def save(global_step, state_dict, optim_dict, save_path):
        state = {
            'global_step': global_step,
            'state_dict': state_dict,
            'optim_dict': optim_dict
        }
        torch.save(state, save_path)

    @staticmethod
    def load(load_path):
        if torch.cuda.is_available():
            ckpt = torch.load(load_path)
        else:
            ckpt = torch.load(load_path, map_location='cpu')
        return ckpt

class Resnet_embed(nn.Module):
    def __init__(self, num_class, use_pretrain=False):
        super(Resnet_embed, self).__init__()
        self.resnet = models.resnet50(pretrained=True) if use_pretrain else models.resnet50()
        self.adavg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Sequential(
            nn.Linear(self.resnet.fc.in_features, 512),
        )
    def forward(self, input):
        for i, (name, module) in enumerate(self.resnet._modules.items()):
            if name=='avgpool':
                break
            input = module(input)
        input = self.adavg_pool(input)
        embed = input.view(input.size(0), -1)
        embed = self.fc[0](embed)
        return embed

    @staticmethod
    def save(global_step, state_dict, optim_dict, save_path):
        state = {
            'global_step': global_step,
            'state_dict': state_dict,
            'optim_dict': optim_dict
        }
        torch.save(state, save_path)

    @staticmethod
    def load(load_path):
        if torch.cuda.is_available():
            ckpt = torch.load(load_path)
        else:
            ckpt = torch.load(load_path, map_location='cpu')
        return ckpt


if __name__ == '__main__':
    m = models.resnet152(pretrained=True)
    print(m)
    print(dir(m))
    for i, (name, module) in enumerate(m._modules.items()):
        print(i, name, module)
