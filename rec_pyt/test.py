# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import dataloader
from networks import Resnet
import numpy as np
from collections import OrderedDict
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# Hyper Parameters
batch_size = 36
aug_mode = 4
model_path = 'model/model_' + str(aug_mode) + '.pkl'
data_path = "/data/yushang/dataset/train/"
output_path = 'output/result.txt'
num_class = 203094
device_ids = [0,]

def softmax(vec):
    tmp = np.max(vec)
    x = vec - tmp
    x = np.exp(x)
    tmp = np.sum(x)
    x /= tmp
    return x


def ACC(result):
    pos = 0
    neg = 0
    for id,pred,confidence,truth in result:
        if pred == truth:
            pos += 1
        else:
            neg += 1
    rate = float(pos) / float(pos + neg)
    return rate


def GAP(result):
    result_desc = sorted(result, key=lambda x:x[2], reverse=True)
    correct = 0
    wrong = 0
    posi = 0
    sum_p = 0
    for id, pred, confidence, truth in result_desc:
        if pred == truth and pred is not None and truth is not None:
            correct += 1
        else:
            wrong += 1
        if pred == truth and pred is not None and truth is not None:
            sum_p += float(correct) / float(correct + wrong)
        if truth is not None:
            posi += 1
    return sum_p / posi

def save(result):
    with open(output_path, 'w') as f:
        result_desc = sorted(result, key=lambda x: x[2], reverse=True)
        for id, pred, confidence, truth in result_desc:
            f.write(id + ',' + str(pred) + ',' + str(confidence) + ',' + str(truth) + '\n')


def main():
    net0 = Resnet(num_class=num_class, use_pretrain=False).to(device)
    net = torch.nn.DataParallel(net0, device_ids=device_ids).to(device)
    ckpt = net0.load(model_path)
    global_step = ckpt['global_step']
    dist_state_dict = OrderedDict()
    for k, v in ckpt['state_dict'].items():
        name = k.replace('module.', '')  # remove the 'module' prefix.
        dist_state_dict[name] = v
    net.module.load_state_dict(dist_state_dict)
    print('load from last model')
    net.eval()
    print('init net')

    test_dataloader = dataloader.get_test_data_loader(path=data_path,metafile="train.csv",num_class=num_class,batch_size=batch_size)

    result = []
    for i, (images, labels, ids) in enumerate(test_dataloader):
        images = Variable(images).to(device)
        labels = Variable(labels.long()).to(device)
        logit, embed, loss = net(images, labels)
        for k in range(len(labels)):
            truth = labels.cpu().numpy()[k]
            id = ids[k]
            k_th_vec = logit[k,:].data.cpu().numpy()
            k_th_soft = softmax(k_th_vec)
            pred = np.argmax(k_th_vec)
            confidence = k_th_soft[pred]
            result.append([id,pred,confidence,truth])
        print(i)

    print("ACC: " + str(ACC(result)))
    print("GAP: " + str(GAP(result)))
    save(result)









if __name__ == '__main__':
    main()
