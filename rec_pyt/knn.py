# -*- coding: UTF-8 -*-
import os
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
train_dir = '/data/yushang/tmp/output/imagenet_embed/'
test_dir = '/data/yushang/tmp/output/imagenet_embed_test/'
# train_dir = 'output/imagenet_embed/'
# test_dir = 'output/imagenet_embed_test/'

# X = [[0], [1], [2], [3]]
# y = [0, 0, 1, 1]

# neigh = KNeighborsClassifier(n_neighbors=3)
# neigh.fit(X, y)
# print(neigh.predict([[1.1]]))
# print(neigh.predict_proba([[0.9]]))

with open('/data/yushang/dataset/train/train.csv') as f:
    meta_data = [(id, int(lid)) for id, url, lid in [line.split(',') for line in f.readlines()[1:]]]
id2cat = dict(meta_data)

X = []
y = []
for fname in os.listdir(train_dir):
    temp = np.load(os.path.join(train_dir, fname))
    for k,v in temp.items():
        X.append(v)
        y.append(id2cat[k.split('_')[1]])
    print(len(y))

neigh = KNeighborsClassifier(n_neighbors=1, n_jobs=30)
neigh.fit(X, y)

with open('output/knnresult-1.txt', 'w') as f:
    f.write('id,landmarks\n')
    for fname in os.listdir(test_dir):
        temp = np.load(os.path.join(test_dir, fname))
        klist = []
        vlist = []
        for k, v in temp.items():
            klist.append(k)
            vlist.append(v)
        proba = neigh.predict_proba(vlist)
        for i in range(len(klist)):
            k = klist[i]
            cat = np.argmax(proba[i])
            confidence = proba[i][cat]
            print(str(k.split('_')[1]) + ',' + str(cat) + ' ' + str(confidence))
            f.write(str(k.split('_')[1]) + ',' + str(cat) + ' ' + str(confidence) + '\n')
