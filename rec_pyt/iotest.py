# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import dataloader
from networks import Resnet
import os
# Hyper Parameters
num_epochs = 2
batch_size = 192
learning_rate = 0.001
is_new_model = True
aug_mode = 4
model_path = 'model/model_' + str(aug_mode) + '.pkl'
data_path = "/data/yushang/dataset/train/"
num_class = 203094
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

from dataloader import *
def get_train_data_loader(path, metafile, num_class, batch_size = 8, aug_mode = 0):
    transform_method = transform_method_list_train[aug_mode]
    dataset = GDataset(path, metafile, transform=transform_method, num_class=num_class, filter_mode=1)
    return DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=4)

train_dataloader = get_train_data_loader(path=data_path, metafile="train.csv", num_class=num_class,
                                                    batch_size=batch_size, aug_mode=aug_mode)

ts = time.time()
for epoch in range(num_epochs):
    for i, (images, labels, id) in enumerate(train_dataloader):
        images = Variable(images).to(device)
        labels = Variable(labels.long()).to(device)
        if (i + 1) % 10 == 0:
            speed = '%.2f' % (len(labels) * 10.0 / (time.time() - ts))
            ts = time.time()
            print("epoch:", epoch, "step:", i, 'samples/sec:', speed)
