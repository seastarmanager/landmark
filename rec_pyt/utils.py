import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributed as dist

import torchvision
import torchvision.transforms as transforms
import torchvision.models as models

import numpy as np
import random
from PIL import Image
# import matplotlib.pyplot as plt

cnn_normalization_mean = [0.485, 0.456, 0.406]
cnn_normalization_std = [0.229, 0.224, 0.225]
tensor_normalizer = transforms.Normalize(mean=cnn_normalization_mean, std=cnn_normalization_std)
epsilon = 1e-5

# 图片读入后的tensor的变换
#resize_width = 320
crop_width = 224

def randomResize(img, lmin,lmax, keep_aspect_ratio=False):
    if keep_aspect_ratio:
        iw, ih = img.size
        short = random.randint(lmin, lmax)
        if iw <= ih:
            w = short
            h = int(float(w) / iw * ih)
        else:
            h = short
            w = int(float(h) / ih * iw)
    else:
        w = random.randint(lmin, lmax)
        h = w
    return img.resize((w, h))

def randomChannel(img):
    r, g, b = img.split()
    p = random.randint(0, 9)
    if p == 5:
        img = Image.merge('RGB', (r, b, g))
    elif p == 6:
        img = Image.merge('RGB', (g, r, b))
    elif p == 7:
        img = Image.merge('RGB', (g, b, r))
    elif p == 8:
        img = Image.merge('RGB', (b, g, r))
    elif p == 9:
        img = Image.merge('RGB', (b, r, g))
    return img

def trans_generator(color=False, rotate=False, keep_aspect_ratio=False, random_crop=False):
    oplist = []
    if color:
        oplist.append(transforms.Lambda(lambda img: randomChannel(img)))
        oplist.append(transforms.ColorJitter(0.2,0.2,0.2,0.1))
    if rotate:
        oplist.append(transforms.RandomRotation(20))
    if random_crop:
        if keep_aspect_ratio:
            oplist.append(transforms.Lambda(lambda img: randomResize(img, 228, 288, True)))
        else:
            oplist.append(transforms.Lambda(lambda img: randomResize(img, 228, 320)))
        oplist.append(transforms.RandomCrop(crop_width))
        oplist.append(transforms.RandomHorizontalFlip())
    else:
        if keep_aspect_ratio:
            oplist.append(transforms.Lambda(lambda img: randomResize(img, 224, 224, True)))
        else:
            oplist.append(transforms.Lambda(lambda img: randomResize(img, 224, 224)))
        oplist.append(transforms.CenterCrop(crop_width))
    oplist.append(transforms.ToTensor())
    oplist.append(tensor_normalizer)
    return transforms.Compose(oplist)


# data_transform_train = transforms.Compose([
#     transforms.Resize([resize_width,resize_width]),
#     transforms.RandomCrop(crop_width),
#     transforms.ToTensor(),
#     tensor_normalizer,
# ])
# data_transform_test = transforms.Compose([
#     transforms.Resize([resize_width,resize_width]),
#     transforms.CenterCrop(crop_width),
#     transforms.ToTensor(),
#     tensor_normalizer,
# ])

# def preprocess_image(image, target_width=None):
#     """输入 PIL.Image 对象，输出标准化后的四维 tensor"""
#     if target_width:
#         t = transforms.Compose([
#             transforms.Resize(target_width),
#             transforms.CenterCrop(target_width),
#             transforms.ToTensor(),
#             tensor_normalizer,
#         ])
#     else:
#         t = transforms.Compose([
#             transforms.ToTensor(),
#             tensor_normalizer,
#         ])
#     return t(image).unsqueeze(0)
#
#
# def read_image(path, target_width=None):
#     """输入图像路径，输出标准化后的四维 tensor"""
#     image = Image.open(path)
#     return preprocess_image(image, target_width)

class Partition(object):

    def __init__(self, data, index):
        self.data = data
        self.index = index

    def __len__(self):
        return len(self.index)

    def __getitem__(self, index):
        data_idx = self.index[index]
        return self.data[data_idx]

class DataPartitioner(object):

    def __init__(self, data, sizes=[0.7, 0.2, 0.1], seed=1234):
        self.data = data
        self.partitions = []
        rng = random.Random()
        rng.seed(seed)
        data_len = len(data)
        indexes = [x for x in range(0, data_len)]
        rng.shuffle(indexes)

        for frac in sizes:
            part_len = int(frac * data_len)
            self.partitions.append(indexes[0:part_len])
            indexes = indexes[part_len:]

    def use(self, partition):
        return Partition(self.data, self.partitions[partition])

def partition_dataset(dataset, batch_size, shuffle):
    size = dist.get_world_size()
    bsz = batch_size / float(size)
    partition_sizes = [1.0 / size for _ in range(size)]
    partition = DataPartitioner(dataset, partition_sizes)
    partition = partition.use(dist.get_rank())
    train_set = torch.utils.data.DataLoader(partition,
                                         batch_size=bsz,
                                         shuffle=shuffle)
    return train_set, bsz

