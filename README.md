# landmark

CUDA_VISIBLE_DEVICES=7 python -u train.py >> log/train_0.log

CUDA_VISIBLE_DEVICES=6 python predict_imagenet.py

CUDA_VISIBLE_DEVICES=0,1,2,3,4,5 python -u train.py >> log/train_4.log

CUDA_VISIBLE_DEVICES=6 python iotest.py

pip install torch-encoding

CUDA_VISIBLE_DEVICES=6 python -u test.py >> log/test_4.log



cosine:

CUDA_VISIBLE_DEVICES=6,7 python -u train.py >> log/train_4.log

CUDA_VISIBLE_DEVICES=7 python -u train.py >> log/train_4.log

CUDA_VISIBLE_DEVICES=7 python -u test.py >> log/test_4.log

CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7 python -u train.py >> log/train_0.log 2>&1



CUDA_VISIBLE_DEVICES=7 python3 -u predict_embedding.py >> log/predict_test_0.log 2>&1