# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import dataloader
from networks import Trinet, EmbedNet
from collections import OrderedDict
import numpy as np
import os
# Hyper Parameters
batch_size = 32
aug_mode = 4
model_path = 'model/model_' + str(aug_mode) + '.pkl'
embed_path = 'model/model_' + str(aug_mode) + '_embed.pkl'
data_path = "/data/yushang/dataset/test/test1/"
num_class = 203094
device_ids = [0,]

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
def main():
    net0 = Trinet(num_class=num_class, use_pretrain=False).to(device)
    net_emb = EmbedNet(num_class=num_class, use_pretrain=False).to(device)
    ckpt = net0.load(model_path)
    global_step = ckpt['global_step']
    dist_state_dict = OrderedDict()
    for k, v in ckpt['state_dict'].items():
        name = k.replace('module.', '')  # remove the 'module' prefix.
        dist_state_dict[name] = v
    net0.load_state_dict(dist_state_dict)
    ckpt_emb = net0.load(embed_path)
    net_emb.load_state_dict(ckpt_emb['state_dict'])
    print('load from last model')

    net0.eval()
    net_emb.eval()
    print('init net')

    # Train the Model
    test_dataloader = dataloader.get_test1_data_loader(path=data_path,metafile="",num_class=num_class,
                                                       batch_size=batch_size)
    cat_weight = net_emb.cat_embed.weight.to(device)
    ts = time.time()
    result = []
    for i, (img_embed, labels, ids) in enumerate(test_dataloader):
        img_embed = Variable(img_embed).to(device)
        labels = Variable(labels.long()).to(device)
        cat_embed = net_emb(labels).to(device)
        pic_embed, loss = net0(img_embed, cat_embed, cat_embed)  #loss不重要，主要为了得到pic_embed
        for k in range(len(labels)):
            id = ids[k]
            k_th_vec = pic_embed[k, :].data
            img_embed_mat = k_th_vec.expand_as(cat_weight)
            #cos_sim = F.cosine_similarity(img_embed_mat, cat_weight)
            cos_sim = - F.pairwise_distance(img_embed_mat, cat_weight)
            cos_sim_score = cos_sim.cpu().detach().numpy()
            top5 = torch.topk(cos_sim, 5)[0]
            top5_softmax = F.softmax(top5 - torch.min(top5).expand_as(top5)).cpu().detach().numpy()
            #print(cos_sim_score)
            #print(top5)
            cat_pred = np.argmax(cos_sim_score)
            cat_score = top5_softmax[0]
            result.append([id, str(cat_pred), str(cat_score)])
            print(result[-1])

        if (i + 1) % 10 == 0:
            speed = '%.2f' % (len(labels) * 10.0 / (time.time() - ts))
            ts = time.time()
            print("step:", i, 'samples/sec:', speed)

    result.sort(key=lambda x: x[2], reverse=True)
    with open('output/pair_result.txt', 'w') as f:
        f.write('id,landmarks\n')
        for id, cat_pred, cat_score in result:
            f.write(id + ',' + cat_pred + ' ' + cat_score + '\n')










if __name__ == '__main__':
    main()
