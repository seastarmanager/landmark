# -*- coding: UTF-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class EmbedNet(nn.Module):
    def __init__(self, num_class, use_pretrain=False):
        super(EmbedNet, self).__init__()
        self.cat_embed = nn.Embedding(num_class, 2048)

    def forward(self, y):
        cat_embed = self.cat_embed(y)
        return cat_embed

class Trinet(nn.Module):
    def __init__(self, num_class, use_pretrain=False):
        super(Trinet, self).__init__()
        self.fc = nn.Sequential(nn.Linear(2048, 2048), nn.ReLU(),
                                nn.Linear(2048, 2048), nn.ReLU(),
                                nn.Linear(2048, 2048), nn.ReLU(),
                                nn.Linear(2048, 2048))

    def forward(self, x, cat_embed, wrong_embed):
        pic_embed = self.fc(x)
        #criterion = nn.CosineSimilarity()
        criterion = nn.PairwiseDistance()
        cosp = criterion(pic_embed, cat_embed)
        cosn = criterion(pic_embed, wrong_embed)
        #loss = cosn - cosp + 0.1
        loss = cosp - cosn + 0.5
        loss = F.relu(loss, inplace=True)
        loss = torch.unsqueeze(loss, 0)
        return pic_embed, loss

    @staticmethod
    def save(global_step, state_dict, optim_dict, save_path):
        state = {
            'global_step': global_step,
            'state_dict': state_dict,
            'optim_dict': optim_dict
        }
        torch.save(state, save_path)

    @staticmethod
    def load(load_path):
        if torch.cuda.is_available():
            ckpt = torch.load(load_path)
        else:
            ckpt = torch.load(load_path, map_location='cpu')
        return ckpt


