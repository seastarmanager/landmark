# -*- coding: UTF-8 -*-
import os
import torch
from torch.utils.data import DataLoader,Dataset
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as dset
import numpy as np
import random

class GDataset(Dataset):
    def __init__(self, folder, metafile, filter_mode=0):
        with open(os.path.join(folder, metafile)) as f:
            self.meta_data = [(id, int(lid)) for id, url, lid in [line.strip().split(',') for line in f.readlines()[1:]]]
        self.id2cat = dict(self.meta_data)
        self.idx_embed = {}

    def __len__(self):
        return 4132914

    def __getitem__(self, idx):
        if idx not in self.idx_embed:
            f_idx = idx // 200
            #fname = '/data/yushang/landmark/rec_pyt/output/imagenet_embed/' + str(f_idx) + '.npz'
            fname = '/data/yushang/tmp/output/imagenet_embed/' + str(f_idx) + '.npz'
            temp = np.load(fname)
            for i, (k, v) in enumerate(temp.items()):
                self.idx_embed[f_idx * 200 + i] = (k.split('_')[1], v)
        id, emb = self.idx_embed[idx]
        label = self.id2cat[id]
        label_wrong = random.randint(0, 203094 - 1)
        if label==label_wrong:
            label_wrong = random.randint(0, 203094 - 1)
        return emb, label, id, label_wrong

def get_train_data_loader(path, metafile, batch_size = 8):
    dataset = GDataset(path, metafile)
    return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=0, pin_memory=True)

class TDataset(Dataset):
    def __init__(self, folder, metafile, transform=None, num_class=0):
        self.folder = folder
        self.train_image_file_paths = [os.path.join(folder, fname) for fname in os.listdir(folder)]
        self.idx_embed = {}

    def __len__(self):
        return 112729

    def __getitem__(self, idx):
        if idx not in self.idx_embed:
            f_idx = idx // 200
            fname = '/data/yushang/landmark/rec_pyt/output/imagenet_embed_test/' + str(f_idx) + '.npz'
            temp = np.load(fname)
            for i, (k, v) in enumerate(temp.items()):
                self.idx_embed[f_idx * 200 + i] = (k.split('_')[1], v)
        id, emb = self.idx_embed[idx]
        return emb, 0, id

def get_test1_data_loader(path, metafile, num_class, batch_size = 1):
    dataset = TDataset(path, metafile, num_class=num_class)
    return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=0)

