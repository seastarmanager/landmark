# -*- coding: UTF-8 -*-
import numpy as np
import os
import torch
import dataloader
# Hyper Parameters
batch_size = 32
aug_mode = 4
model_path = 'model/model_' + str(aug_mode) + '.pkl'
embed_path = 'model/model_' + str(aug_mode) + '_embed.pkl'
data_path = "/data/yushang/dataset/test/test1/"
num_class = 203094
device_ids = [0,]

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def main():
    # 测试集放到显存中
    test_dataloader = dataloader.get_test1_data_loader(path=data_path,metafile="",num_class=num_class,
                                                       batch_size=batch_size)
    test_img_embed_list = []
    test_id_list = None
    for i, (img_embed, labels, ids) in enumerate(test_dataloader):
        if test_id_list is not None:
            test_id_list.extend(ids)
        else:
            test_id_list = ids
        test_img_embed_list.append(img_embed)
    test_img_tensor = torch.cat(test_img_embed_list, dim=0).to(device)







if __name__ == '__main__':
    main()

