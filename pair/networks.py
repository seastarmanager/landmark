# -*- coding: UTF-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
import random
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class Resnet(nn.Module):
    def __init__(self, num_class, use_pretrain=False):
        super(Resnet, self).__init__()
        self.resnet = models.resnet50(pretrained=True) if use_pretrain else models.resnet50()
        self.resnet = self.resnet
        self.adavg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Sequential(nn.Linear(self.resnet.fc.in_features, 2048), nn.ReLU(),
                                nn.Linear(2048, 2048),  nn.ReLU(),
                                nn.Linear(2048, 2048),  nn.ReLU(),
                                nn.Linear(2048, 512))

    def forward(self, x, cat_embed, wrong_embed):
        #criterion = nn.CosineSimilarity()
        criterion = nn.PairwiseDistance()
        for i, (name, module) in enumerate(self.resnet._modules.items()):
            if name=='avgpool':
                break
            x = module(x)
        x = self.adavg_pool(x)
        pic_embed = x.view(x.size(0), -1)
        pic_embed = self.fc(pic_embed)
        #cat_embed_shuffle = cat_embed[torch.randperm(cat_embed.size()[0])]
        cosp = criterion(pic_embed, cat_embed)
        cosn = criterion(pic_embed, wrong_embed)
        #loss = cosn - cosp + 0.1
        loss = cosp - cosn + 0.5
        loss = F.relu(loss, inplace=True)
        loss = torch.unsqueeze(loss, 0)
        return pic_embed, loss

    @staticmethod
    def save(global_step, state_dict, optim_dict, save_path):
        state = {
            'global_step': global_step,
            'state_dict': state_dict,
            'optim_dict': optim_dict
        }
        torch.save(state, save_path)

    @staticmethod
    def load(load_path):
        if torch.cuda.is_available():
            ckpt = torch.load(load_path)
        else:
            ckpt = torch.load(load_path, map_location='cpu')
        return ckpt

class Resnet_embed(nn.Module):
    def __init__(self, num_class, use_pretrain=False):
        super(Resnet_embed, self).__init__()
        self.resnet = models.resnet50(pretrained=True) if use_pretrain else models.resnet50()
        self.resnet = self.resnet
        self.adavg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Sequential(nn.Linear(self.resnet.fc.in_features, 2048), nn.ReLU(),
                                nn.Linear(2048, 2048),  nn.ReLU(),
                                nn.Linear(2048, 2048),  nn.ReLU(),
                                nn.Linear(2048, 512))

    def forward(self, x):
        for i, (name, module) in enumerate(self.resnet._modules.items()):
            if name=='avgpool':
                break
            x = module(x)
        x = self.adavg_pool(x)
        pic_embed = x.view(x.size(0), -1)
        pic_embed = self.fc(pic_embed)
        return pic_embed

    @staticmethod
    def save(global_step, state_dict, optim_dict, save_path):
        state = {
            'global_step': global_step,
            'state_dict': state_dict,
            'optim_dict': optim_dict
        }
        torch.save(state, save_path)

    @staticmethod
    def load(load_path):
        if torch.cuda.is_available():
            ckpt = torch.load(load_path)
        else:
            ckpt = torch.load(load_path, map_location='cpu')
        return ckpt

class EmbedNet(nn.Module):
    def __init__(self, num_class, use_pretrain=False):
        super(EmbedNet, self).__init__()
        self.cat_embed = nn.Embedding(num_class, 512)

    def forward(self, y):
        cat_embed = self.cat_embed(y)
        return cat_embed

class NegLoss(nn.Module):
    def __init__(self, num_class, num_neg):
        super(NegLoss, self).__init__()
        self.num_class = num_class
        self.num_neg = num_neg

    def forward(self, img_embeds, cat_embeds, wrong_embeds_list):
        criterion = nn.PairwiseDistance()
        distp = criterion(img_embeds, cat_embeds)
        losslist = []
        for i in range(self.num_neg):
            wrong_embed = wrong_embeds_list[i].expand_as(img_embeds)
            distn = criterion(img_embeds, wrong_embed)
            loss = distp - distn + 0.5
            loss = F.relu(loss, inplace=True)
            losslist.append(loss)
        totalloss_mean = sum(losslist)/self.num_neg
        loss_matrix = torch.cat([torch.unsqueeze(l,1) for l in losslist],1)
        zcount = torch.sum(torch.eq(loss_matrix, 0))
        mat_count = loss_matrix.shape[0] * loss_matrix.shape[1]
        totalloss = totalloss_mean / (mat_count - zcount) * mat_count

        toploss = torch.topk(loss_matrix, 10)[0]
        toploss = torch.mean(toploss, dim=1)
        return totalloss, toploss, mat_count - zcount, totalloss_mean.mean()

if __name__ == '__main__':
    m = models.resnet152(pretrained=True)
    print(m)
    print(dir(m))
    for i, (name, module) in enumerate(m._modules.items()):
        print(i, name, module)
