# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from dataloader import GDataset, T2Dataset, trans_train, trans_test
from utils import tensor_normalizer
import numpy as np
import torch.nn.functional as F
from collections import OrderedDict
from dataloader import *
from networks import Resnet, EmbedNet, Resnet_embed
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# Hyper Parameters
mode = "test"
batch_size = 2200
aug_mode = 0
model_path = 'model/model_' + str(aug_mode) + '.pkl'
embed_path = 'model/model_' + str(aug_mode) + '_embed.pkl'
num_class = 203094

def eval_train_data(net, transform_method):
    dataset = GDataset("/data/dataset/google-landmark/", "train.csv", transform=transform_method, filter_mode=0)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)
    embed_list = []
    embed_dict = {}
    part = 0
    for i, (images, labels, ids, labels_wrong) in enumerate(dataloader):
        images = Variable(images).to(device)
        #labels = Variable(labels).to(device)
        #cat_embed = net_emb(labels).to(device)
        img_embed = net(images)
        for k in range(len(labels)):
            id = ids[k]
            print(i, id)
            embed_k = img_embed[k,:].data.cpu().numpy()
            embed_dict[id] = embed_k
    torch.save(embed_dict, 'output/cat_embed_train.pkl')
            # embed_list.append([id, embed_k])
            # if len(embed_list) >= 200:
            #     eval("np.savez('output/net_embed/' + str(part),"
            #         + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
            #                     for j, (id, embed_k) in enumerate(embed_list)]) + ')')
            #     part += 1
            #     embed_list = []
    # eval("np.savez('output/net_embed/' + str(part),"
    #      + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
    #                  for j, (id, embed_k) in enumerate(embed_list)]) + ')')

def save(result):
    with open('output/pair_result.txt', 'w') as f:
        result_desc = sorted(result, key=lambda x: x[2], reverse=True)
        for id, pred, confidence in result_desc:
            f.write(id + ',' + str(pred) + ' ' + str(confidence) + '\n')

def eval_test_data(net, net_emb, transform_method):
    dataset = T2Dataset("/data/dataset/google-landmark-test2/", "test2.csv", transform=transform_method)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)
    result = []
    embed_list = []
    embed_dict = {}
    part = 0
    cat_weight = net_emb.cat_embed.weight.to(device)
    for i, (images, labels, ids) in enumerate(dataloader):
        images = Variable(images).to(device)
        labels = Variable(labels).to(device)
        cat_embed = net_emb(labels).to(device)
        img_embed, loss = net(images, cat_embed, cat_embed)
        for k in range(len(labels)):
            id = ids[k]
            print(i, id)
            k_th_vec = img_embed[k, :].data
            img_embed_mat = k_th_vec.expand_as(cat_weight)
            #cos_sim = - F.cosine_similarity(img_embed_mat, cat_weight)
            dist = F.pairwise_distance(img_embed_mat, cat_weight)
            dist_score = dist.cpu().detach().numpy()
            top5 = torch.topk(dist, 2, largest=False)[0]
            top5_softmax = F.softmax(torch.max(top5).expand_as(top5) - top5).cpu().detach().numpy()
            #dist_confi = (1.0 / dist).cpu().detach().numpy()
            # print(cos_sim_score)
            # print(top5)
            cat_pred = np.argmin(dist_score)
            cat_score = top5_softmax[0]
            #cat_score = dist_confi[cat_pred]
            result.append([id, str(cat_pred), str(cat_score)])
            print(result[-1])
            embed_k = k_th_vec.cpu().numpy()
            embed_dict[id] = embed_k
    torch.save(embed_dict, 'output/cat_embed_test.pkl')
    save(result)
    #         embed_list.append([id, embed_k])
    #         if len(embed_list) >= 200:
    #             eval("np.savez_compressed('output/net_embed_test/' + str(part),"
    #                 + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
    #                             for j, (id, embed_k) in enumerate(embed_list)]) + ')')
    #             part += 1
    #             embed_list = []
    # eval("np.savez_compressed('output/net_embed_test/' + str(part),"
    #      + ','.join(['id_' + id + '=embed_list[' + str(j) + '][1]'
    #                  for j, (id, embed_k) in enumerate(embed_list)]) + ')')


def eval_test_multi_crop(net, net_emb, transform_method):
    trans_multi = transforms.Compose([
        transforms.Lambda(lambda img: randomResize(img, 228, 320)),
        transforms.RandomCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
    dataset = T2Dataset("/data/dataset/google-landmark-test2/", "test2.csv", transform=transform_method)
    dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)
    dataloader_list = [dataloader, ]
    for j in range(5):
        dataset = T2Dataset("/data/dataset/google-landmark-test2/", "test2.csv", transform=trans_multi)
        dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)
        dataloader_list.append(dataloader)
    cat_weight = net_emb.cat_embed.weight.to(device)
    dist_dict = {}
    result = []
    for dataloader in dataloader_list:
        for i, (images, labels, ids) in enumerate(dataloader):
            images = Variable(images).to(device)
            labels = Variable(labels).to(device)
            cat_embed = net_emb(labels).to(device)
            img_embed, loss = net(images, cat_embed, cat_embed)
            print(i)
            for k in range(len(labels)):
                id = ids[k]
                k_th_vec = img_embed[k, :].data
                img_embed_mat = k_th_vec.expand_as(cat_weight)
                dist = F.pairwise_distance(img_embed_mat, cat_weight)
                dist_score = dist.cpu().detach()
                if id not in dist_dict:
                    dist_dict[id] = dist_score
                else:
                    dist_dict[id] = dist_dict[id] + dist_score
    for id, dist_score in dist_dict.items():
        dist_score = dist_score / float(len(dataloader_list))
        dist = dist_score.to(device)
        top5 = torch.topk(dist, 2, largest=False)[0]
        top5_softmax = F.softmax(torch.max(top5).expand_as(top5) - top5).cpu().detach().numpy()
        cat_pred = np.argmin(dist_score.numpy())
        cat_score = top5_softmax[0]
        result.append([id, str(cat_pred), str(cat_score)])
        print(result[-1])
    save(result)






def main():
    if mode == "train":
        net0 = Resnet_embed(num_class=num_class, use_pretrain=False).to(device)
    else:
        net0 = Resnet(num_class=num_class, use_pretrain=False).to(device)
    net = net0
    #net = torch.nn.DataParallel(net0, device_ids=device_ids).to(device)
    ckpt = net0.load(model_path)
    dist_state_dict = OrderedDict()
    for k, v in ckpt['state_dict'].items():
        name = k.replace('module.', '')  # remove the 'module' prefix.
        dist_state_dict[name] = v
    net.load_state_dict(dist_state_dict)
    net.eval()
    #net.module.load_state_dict(dist_state_dict)
    if mode != 'train':
        net_emb = EmbedNet(num_class=num_class, use_pretrain=False)
        net_emb = net_emb.to(device)
        ckpt_emb = net0.load(embed_path)
        net_emb.load_state_dict(ckpt_emb['state_dict'])
        net_emb.eval()
    print('init net')
    #transform_method = transform_method_list_test[aug_mode]

    if mode == "train":
        eval_train_data(net, trans_test)
    elif mode == "test":
        eval_test_data(net, net_emb, trans_test)
    elif mode == "test_multicrop":
        eval_test_multi_crop(net, net_emb, trans_test)



if __name__ == '__main__':
    with torch.no_grad():
        main()
