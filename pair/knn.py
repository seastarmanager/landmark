# -*- coding: UTF-8 -*-
import os
import numpy as np
import torch
from sklearn.neighbors import KNeighborsClassifier
train_dir = 'output/net_embed_train.pkl'
test_dir = 'output/net_embed_test.pkl'

with open('/data/yushang/dataset/train/train.csv') as f:
    meta_data = [(id, int(lid)) for id, url, lid in [line.split(',') for line in f.readlines()[1:]]]
id2cat = dict(meta_data)

X = []
y = []

ckpt = torch.load(train_dir, map_location='cpu')

for id, x_embed in ckpt.items():
    X.append(x_embed)
    y.append(id2cat[id])

neigh = KNeighborsClassifier(n_neighbors=10, n_jobs=30)
neigh.fit(X, y)

with open('output/knnresult.txt', 'w') as f:
    f.write('id,landmarks\n')
    ckpt = torch.load(test_dir, map_location='cpu')
    klist = []
    vlist = []
    for id, x_embed in ckpt.items():
        klist.append(id)
        vlist.append(x_embed)
    proba = neigh.predict_proba(vlist)
    for i in range(len(klist)):
        k = klist[i]
        cat = np.argmax(proba[i])
        confidence = proba[i][cat]
        print(str(k.split('_')[1]) + ',' + str(cat) + ' ' + str(confidence))
        f.write(str(k.split('_')[1]) + ',' + str(cat) + ' ' + str(confidence) + '\n')
