# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import dataloader
from networks import Resnet, EmbedNet, NegLoss
from collections import OrderedDict
import torch.nn.functional as F
import numpy as np
import os
import random
# Hyper Parameters
num_epochs = 40
batch_size = 1330
learning_rate = 0.001
is_new_model = False
neg_num = 2000
tri_hard = False
aug_mode = 0
model_path = 'model/model_' + str(aug_mode) + '.pkl'
embed_path = 'model/model_' + str(aug_mode) + '_embed.pkl'
#data_path = "/data/yushang/dataset/train/"
data_path = "/data/dataset/google-landmark/"
num_class = 203094
num_samples = 4132914
device_ids = [0, 1, 2, 3, 4, 5, 6]

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
def main():
    net0 = Resnet(num_class=num_class, use_pretrain=is_new_model)
    net = torch.nn.DataParallel(net0, device_ids=device_ids).to(device)
    #net = parallel.DataParallelModel(net0, device_ids=device_ids).to(device)
    net_emb = EmbedNet(num_class=num_class, use_pretrain=is_new_model).to(device)
    #net_emb = torch.nn.DataParallel(net_emb, device_ids=device_ids).to(device)
    net_neg = NegLoss(num_class=num_class, num_neg=neg_num).to(device)
    #net_neg = torch.nn.DataParallel(net_neg, device_ids=device_ids).to(device)
    if is_new_model:
        global_step = 0
        print('train from new model')
    else:
        ckpt = net0.load(model_path)
        global_step = ckpt['global_step']
        dist_state_dict = OrderedDict()
        for k, v in ckpt['state_dict'].items():
            name = k.replace('module.', '')  # remove the 'module' prefix.
            dist_state_dict[name] = v
        net.module.load_state_dict(dist_state_dict)
        ckpt_emb = net0.load(embed_path)
        # dist_state_dict = OrderedDict()
        # for k, v in ckpt_emb['state_dict'].items():
        #     name = k.replace('module.', '')  # remove the 'module' prefix.
        #     dist_state_dict[name] = v
        net_emb.load_state_dict(ckpt_emb['state_dict'])
        print('train from last model')

    net.train()
    net_emb.train()
    print('init net')
    optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate, weight_decay=0.000000)
    optimizer_emb = torch.optim.Adam(net_emb.parameters(), lr=learning_rate, weight_decay=0.000000)

    try:
        optimizer.load_state_dict(ckpt['optim_dict'])
        optimizer_emb.load_state_dict(ckpt_emb['optim_dict'])
        print('load optim_dict')
    except:
        pass

    # Train the Model
    train_dataloader = dataloader.get_train_data_loader(path=data_path,metafile="train.csv",num_class=num_class,
                                                        batch_size=batch_size,aug_mode=aug_mode)
    ts = time.time()
    for epoch in range(num_epochs):
        for i, (images, labels, id ,labels_wrong) in enumerate(train_dataloader):
            # if i == (num_samples-1) / batch_size:
            #     continue
            images = Variable(images).to(device)
            labels_set = set(labels.numpy())
            labels = Variable(labels.long()).to(device)
            labels_wrong = Variable(labels_wrong.long()).to(device)
            cat_embed = net_emb(labels).to(device)
            wrong_embed = net_emb(labels_wrong).to(device)
            img_embed, loss = net(images, cat_embed, wrong_embed)
            if neg_num > 0:
                #wrong_embed_list = []
                # for j in range(neg_num):
                #     offset = random.randint(1, num_class - 1)
                #     wrong_labels = (labels + offset) % num_class
                #     wrong_embed = net_emb(wrong_labels)
                #     wrong_embed_list.append(wrong_embed)
                wrong_labels_candidate = random.sample(range(num_class), k=neg_num + len(labels_set))
                wrong_labels = [wc for wc in wrong_labels_candidate if wc not in labels_set][:neg_num]
                wrong_labels = Variable(torch.tensor(wrong_labels).long()).to(device)
                wrong_embed_list = net_emb(wrong_labels)
                #wrong_embed_list = wrong_embed_list.expand(len(device_ids), -1, -1)
                loss1, loss2, poscount, avgloss = net_neg(img_embed, cat_embed, wrong_embed_list)
                loss_merge = loss1 + loss2
                loss2 = loss2.mean()
            else:
                loss2 = loss.mean()
            #l2loss = F.normalize(img_embed)
            loss = loss_merge.mean()
            # if tri_hard and random.randint(0,2)==0:
            #     val_len = 2000
            #     offset = random.randint(0, num_class - val_len)
            #     cat_weight = net_emb.cat_embed.weight[offset:offset+val_len]
            #     labels_pred_list = []
            #     for k in range(len(labels)):
            #         k_th_vec = img_embed[k, :].data
            #         label = labels[k].cpu().detach().numpy()
            #         img_embed_mat = k_th_vec.expand_as(cat_weight)
            #         similar = - F.pairwise_distance(img_embed_mat, cat_weight)
            #         pred = torch.argmax(similar).cpu().detach().numpy() + offset
            #         if label==pred:
            #             pred = np.array(random.randint(0, num_class - 1))
            #             if label==pred:
            #                 pred = np.array(random.randint(0, num_class - 1))
            #         labels_pred_list.append(pred)
            #     labels_hard = torch.tensor(np.array(labels_pred_list)).to(device)
            #     hard_embed = net_emb(labels_hard)
            #     img_embed, loss_hard = net(images, cat_embed, hard_embed)
            #     loss_hard = loss_hard.mean()
            #     loss = loss + loss_hard
            optimizer.zero_grad()
            optimizer_emb.zero_grad()
            loss.backward()
            optimizer.step()
            optimizer_emb.step()
            global_step += 1
            if (i + 1) % 10 == 0:
                speed = '%.2f' % (len(labels) * 10.0 / (time.time() - ts))
                ts = time.time()
                print("epoch:", epoch, "step:", i, 'global_step:', global_step, 'samples/sec:', speed, "loss:",
                      loss.item(), "hardloss:", loss2.item(), "poscount: ", poscount.item() / float(batch_size),
                      "avgloss: ", avgloss.item())
            if (i + 1) % 200 == 0:
                net0.save(global_step, net.state_dict(), optimizer.state_dict(), model_path)  # current is model.pkl
                net0.save(global_step, net_emb.state_dict(), optimizer_emb.state_dict(), embed_path)
                print("save model")
        print("epoch:", epoch, "step:", i, 'global_step:', global_step, "loss:", loss.item(), "hardloss", loss2.item())
        net0.save(global_step, net.state_dict(), optimizer.state_dict(), model_path)  # current is model.pkl
        net0.save(global_step, net_emb.state_dict(), optimizer_emb.state_dict(), embed_path)
        print("save last model")











if __name__ == '__main__':
    main()
