# -*- coding: UTF-8 -*-
import os
import torch
from torch.utils.data import DataLoader,Dataset
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as dset
from PIL import Image
from utils import *
import math
#device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class GDataset(Dataset):
    def __init__(self, folder, metafile, transform=None, num_class=0, filter_mode=0):
        with open(os.path.join(folder, metafile)) as f:
            self.meta_data = [(id, lid) for id, url, lid in [line.split(',') for line in f.readlines()[1:]]]
        if filter_mode == 1:
            self.meta_data = [e for e in self.meta_data if e[0][-1] != '7']
        elif filter_mode == 2:
            self.meta_data = [e for e in self.meta_data if e[0][-1] == '7']
        def id2path(id):
            return folder + '/'.join(id[:3]) + '/' + id + '.jpg'
        self.train_image_file_paths = [id2path(id) for id, lid in self.meta_data]
        self.transform = transform
        self.num_class = num_class

    def __len__(self):
        return len(self.train_image_file_paths)

    def __getitem__(self, idx):
        image_path = self.train_image_file_paths[idx]
        image = Image.open(image_path)
        if self.transform is not None:
            image = self.transform(image)
        id = self.meta_data[idx][0]
        lid = self.meta_data[idx][1].strip()
        label = int(lid)
        label_wrong = random.randint(0, 203094 - 1)
        if label==label_wrong:
            label_wrong = random.randint(0, 203094 - 1)
        return image, label, id, label_wrong

class BDataset(Dataset):
    def __init__(self, folder, metafile, transform=None, num_class=0, filter_mode=0):
        with open(os.path.join(folder, metafile)) as f:
            self.meta_data = [(id, lid) for id, url, lid in [line.split(',') for line in f.readlines()[1:]]]
        self.data_dict = {}
        def id2path(id):
            return folder + '/'.join(id[:3]) + '/' + id + '.jpg'
        for id, lid in self.meta_data:
            if int(lid) not in self.data_dict:
                self.data_dict[int(lid)] = []
            path = id2path(id)
            self.data_dict[int(lid)].append([id, path])
        self.data_list = []
        for lid, v in self.data_dict.items():
            length = len(v)
            loglen = int(3 * math.log(length)**2 + 1)
            for j in range(min(length, loglen)):  # 1->1 2->2 35->35 40->37 100->47 1000->70 10000->93
                self.data_list.append(lid)
        self.transform = transform
        self.num_class = num_class

    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, idx):
        image_list = self.data_dict[self.data_list[idx]]
        id, image_path = image_list[random.randint(0, len(image_list) - 1)]
        image = Image.open(image_path)
        if self.transform is not None:
            image = self.transform(image)
        lid = self.data_list[idx]
        label = int(lid)
        label_wrong = random.randint(0, 203094 - 1)
        if label==label_wrong:
            label_wrong = random.randint(0, 203094 - 1)
        return image, label, id, label_wrong


# transform_method_list_train = [
#     trans_generator(keep_aspect_ratio=False, random_crop=True),
#     trans_generator(keep_aspect_ratio=False, rotate=True, random_crop=True),
#     trans_generator(keep_aspect_ratio=False, color=True, random_crop=True),
#     trans_generator(keep_aspect_ratio=False, rotate = True, color=True, random_crop=True),
#     trans_generator(keep_aspect_ratio=True, random_crop=True),
#     trans_generator(keep_aspect_ratio=True, rotate=True, random_crop=True),
#     trans_generator(keep_aspect_ratio=True, color=True, random_crop=True),
#     trans_generator(keep_aspect_ratio=True, rotate=True, color=True, random_crop=True),
# ]
#
# transform_method_list_test = [
#     trans_generator(keep_aspect_ratio=False, random_crop=False),
#     trans_generator(keep_aspect_ratio=False, rotate=True, random_crop=False),
#     trans_generator(keep_aspect_ratio=False, color=True, random_crop=False),
#     trans_generator(keep_aspect_ratio=False, rotate = True, color=True, random_crop=False),
#     trans_generator(keep_aspect_ratio=True, random_crop=False),
#     trans_generator(keep_aspect_ratio=True, rotate=True, random_crop=False),
#     trans_generator(keep_aspect_ratio=True, color=True, random_crop=False),
#     trans_generator(keep_aspect_ratio=True, rotate=True, color=True, random_crop=False),
# ]

trans_train = transforms.Compose([
    transforms.Lambda(lambda img: randomResize(img, 228, 320)),
    transforms.RandomCrop(224),
    transforms.RandomHorizontalFlip(),
    #transforms.RandomApply([transforms.ColorJitter(brightness=0.4,contrast=0.4,saturation=0.4,hue=0)],p=0.2),
    #transforms.RandomApply([transforms.RandomRotation(20)],p=0.05),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

trans_test = transforms.Compose([
    transforms.Lambda(lambda img: randomResize(img, 228, 228)),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

def get_train_data_loader(path, metafile, num_class, batch_size = 8, aug_mode = 0):
    #transform_method = transform_method_list_train[aug_mode]
    dataset = BDataset(path, metafile, transform=trans_train, num_class=num_class, filter_mode=0)
    return DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=24, pin_memory=True, drop_last=True)


# def get_test_data_loader(path, metafile, num_class, batch_size = 1, aug_mode = 0):
#     transform_method = transform_method_list_test[aug_mode]
#     dataset = GDataset(path, metafile, transform=transform_method, num_class=num_class, filter_mode=2)
#     return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)



# class TDataset(Dataset):
#     def __init__(self, folder, metafile, transform=None, num_class=0):
#         # with open(os.path.join(folder, metafile)) as f:
#         #     self.meta_data = [id for id, url in [line.split(',') for line in f.readlines()[1:]]]
#         # def id2path(id):
#         #     return folder + id + '.jpg'
#         # self.train_image_file_paths = [id2path(id) for id in self.meta_data]
#         self.folder = folder
#         self.train_image_file_paths = [os.path.join(folder, fname) for fname in os.listdir(folder)]
#         self.transform = transform
#         self.num_class = num_class
#
#     def __len__(self):
#         return len(self.train_image_file_paths)
#
#     def __getitem__(self, idx):
#         image_path = self.train_image_file_paths[idx]
#         image = Image.open(image_path)
#         if (image.mode != 'RGB'):
#             image = image.convert("RGB")
#         if self.transform is not None:
#             image = self.transform(image)
#         id = image_path.split('/')[-1].split('.')[0]
#         label = int(0)
#         return image, label, id
#
# def get_test1_data_loader(path, metafile, num_class, batch_size = 1, aug_mode = 0):
#     #transform_method = transform_method_list_train[aug_mode]
#     dataset = TDataset(path, metafile, transform=trans_test, num_class=num_class)
#     return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8)

class T2Dataset(Dataset):
    def __init__(self, folder, metafile, transform=None, num_class=0):
        with open(os.path.join(folder, metafile)) as f:
            self.meta_data = [id.strip() for id in f.readlines()[1:]]
        def id2path(id):
            return folder + '/'.join(id[:3]) + '/' + id + '.jpg'
        self.train_image_file_paths = [id2path(id) for id in self.meta_data]
        self.transform = transform
        self.num_class = num_class

    def __len__(self):
        return len(self.train_image_file_paths)

    def __getitem__(self, idx):
        image_path = self.train_image_file_paths[idx]
        image = Image.open(image_path)
        if (image.mode != 'RGB'):
            image = image.convert("RGB")
        if self.transform is not None:
            image = self.transform(image)
        id = self.meta_data[idx]
        label = int(0)
        return image, label, id


def get_stage2_data_loader(path, metafile, num_class, batch_size=8):
    dataset = T2Dataset(path, metafile, transform=trans_test, num_class=num_class)
    return DataLoader(dataset, batch_size=batch_size, shuffle=False, num_workers=8, pin_memory=True)


if __name__ == '__main__':
    pass
