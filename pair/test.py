# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import dataloader
from networks import Resnet, EmbedNet
from collections import OrderedDict
import numpy as np
import os
# Hyper Parameters
batch_size = 32
aug_mode = 0
model_path = 'model/model_' + str(aug_mode) + '.pkl'
embed_path = 'model/model_' + str(aug_mode) + '_embed.pkl'
data_path = "/data/yushang/dataset/test/test1/"
num_class = 203094
device_ids = [0,]

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
def main():
    net0 = Resnet(num_class=num_class, use_pretrain=False).to(device)
    net = torch.nn.DataParallel(net0, device_ids=device_ids[:])#.to(device)
    net_emb = EmbedNet(num_class=num_class, use_pretrain=False).to(device)
    #net_emb = torch.nn.DataParallel(net_emb, device_ids=device_ids[:])#.to(device)
    ckpt = net0.load(model_path)
    global_step = ckpt['global_step']
    dist_state_dict = OrderedDict()
    for k, v in ckpt['state_dict'].items():
        name = k.replace('module.', '')  # remove the 'module' prefix.
        dist_state_dict[name] = v
    net.load_state_dict(dist_state_dict)
    ckpt_emb = net0.load(embed_path)
    net_emb.load_state_dict(ckpt_emb['state_dict'])
    print('load from last model')

    net.eval()
    net_emb.eval()
    print('init net')

    # Train the Model
    test_dataloader = dataloader.get_test1_data_loader(path=data_path,metafile="",num_class=num_class,
                                                       batch_size=batch_size, aug_mode=aug_mode)
    cat_weight = net_emb.cat_embed.weight.to(device)
    ts = time.time()
    result = []
    for i, (images, labels, ids) in enumerate(test_dataloader):
        images = Variable(images).to(device)
        labels = Variable(labels.long())#.to(device)
        cat_embed = net_emb(labels).to(device)
        img_embed, loss = net0(images, cat_embed, cat_embed)
        for k in range(len(labels)):
            id = ids[k]
            k_th_vec = img_embed[k, :].data
            img_embed_mat = k_th_vec.expand_as(cat_weight)
            cos_sim = - F.cosine_similarity(img_embed_mat, cat_weight)
            cos_sim_score = cos_sim.cpu().detach().numpy()
            top5 = torch.topk(cos_sim, 5)[0]
            top5_softmax = F.softmax(top5 - torch.min(top5).expand_as(top5)).cpu().detach().numpy()
            print(cos_sim_score)
            print(top5)
            cat_pred = np.argmax(cos_sim_score)
            cat_score = top5_softmax[0]
            result.append([id, str(cat_pred), str(cat_score)])
            print(result[-1])

        if (i + 1) % 10 == 0:
            speed = '%.2f' % (len(labels) * 10.0 / (time.time() - ts))
            ts = time.time()
            print("step:", i, 'samples/sec:', speed)

    result.sort(key=lambda x: x[2], reverse=True)
    with open('output/pair_result.txt', 'w') as f:
        f.write('id,landmarks\n')
        for id, cat_pred, cat_score in result:
            f.write(id + ',' + cat_pred + ' ' + cat_score + '\n')










if __name__ == '__main__':
    main()
