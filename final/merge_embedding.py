# -*- coding: UTF-8 -*-
import torch
import numpy as np

train_pkl_paths = ["/data/dataset/embeddings-landmark/cat_embed_train.pkl",
                   "/data/dataset/embeddings-landmark/softmax_embed_train.pkl",
                   "/data/dataset/embeddings-landmark/triple_embed_train.pkl"]
test_pkl_paths = ["/data/dataset/embeddings-landmark/cat_embed_test.pkl",
                  "/data/dataset/embeddings-landmark/softmax_embed_test.pkl",
                  "/data/dataset/embeddings-landmark/triple_embed_test.pkl"]

def main():
    result_list = []
    mean_list = []
    std_list = []
    for path in train_pkl_paths:
        print('read ' + path)
        ckpt = torch.load(path, map_location='cpu')
        print('sample num: ' + str(len(ckpt)))
        emb_mean = sum(ckpt.values())/float(len(ckpt))
        emb_std = np.sqrt(sum([(v - emb_mean)**2 for v in ckpt.values()])/float(len(ckpt)))
        mean_list.append(emb_mean)
        std_list.append(emb_std)
        for id, v in ckpt.items():
            ckpt[id] = (ckpt[id] - emb_mean) / emb_std
        result_list.append(ckpt)
    result_dict = {}
    for i, id in enumerate(result_list[0].keys()):
        if i % 100000 == 0:
            print('merge ' + str(i) + ' ids')
        result_dict[id] = np.concatenate([ckpt[id] for ckpt in result_list])
    result_list = []
    torch.save(result_dict, 'output/merge_embed_train.pkl')
    print('finished saving train data')
    result_dict = {}
    for i, path in enumerate(test_pkl_paths):
        print('read ' + path)
        ckpt = torch.load(path, map_location='cpu')
        print('sample num: ' + str(len(ckpt)))
        emb_mean = mean_list[i]
        emb_std = std_list[i]
        for id, v in ckpt.items():
            ckpt[id] = (ckpt[id] - emb_mean) / emb_std
        result_list.append(ckpt)
    for i, id in enumerate(result_list[0].keys()):
        if i % 100000 == 0:
            print('merge ' + str(i) + ' ids')
        result_dict[id] = np.concatenate([ckpt[id] for ckpt in result_list])
    torch.save(result_dict, 'output/merge_embed_test.pkl')
    print('finished saving test data')


if __name__ == '__main__':
    main()
