# -*- coding: UTF-8 -*-
import os
import torch
import random
import time
import numpy as np
from torch.utils.data import DataLoader,Dataset
import multiprocessing as mp
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as dset

class GDataset(Dataset):
    def __init__(self, csvfile, datafile, num_class=203094, batch_size=5000, max_per_cat=30):
        with open(csvfile) as f:
            self.meta_data = [(id, int(lid)) for id, url, lid in [line.split(',') for line in f.readlines()[1:]]]
        self.id_dict = {}
        for id, lid in self.meta_data:
            if lid not in self.id_dict:
                self.id_dict[lid] = []
            self.id_dict[lid].append(id)
        ckpt = torch.load(datafile, map_location='cpu')
        self.vec_dict = ckpt
        # for id, vec in ckpt.items():
        #     self.vec_dict[id] = vec
        num_per_epoch = 0
        for lid, idlist in self.id_dict.items():
            num_per_epoch += min(len(idlist), max_per_cat)
        self.batch_size = batch_size
        self.max_per_cat = max_per_cat
        self.num_class = num_class
        self.task_list = mp.Array('i', list(range(num_class)))
        random.shuffle(self.task_list)
        self.offset = mp.Value('i', 0, lock=True)
        self.epoch = mp.Value('i', 0, lock=True)

    def __len__(self):
        return 100000000000

    def __getitem__(self, idx):
        idlist = []
        veclist = []
        labellist = []
        cat_dict = {}
        with self.offset.get_lock():
            epoch = self.epoch.value
            offset = self.offset.value
            if self.offset.value >= self.num_class:
                self.offset.value = 0
                self.epoch.value += 1
                epoch = self.epoch.value
                random.shuffle(self.task_list)
        while len(idlist) < self.batch_size:
            with self.offset.get_lock():
                lid = self.task_list[self.offset.value % self.num_class]
                self.offset.value += 1
                offset = self.offset.value
            candidate_ids = self.id_dict[lid]
            random.shuffle(candidate_ids)
            ids = candidate_ids[:self.max_per_cat]
            idlist.extend(ids)
            for id in ids:
                veclist.append(self.vec_dict[id])
            #cat_dict[lid] = np.array(list(range(len(labellist), min(len(idlist), self.batch_size))))
            labellist.extend([lid]*len(ids))
        idlist = idlist[:self.batch_size]
        veclist = veclist[:self.batch_size]
        labellist = labellist[:self.batch_size]
        vec_mat = np.stack(veclist)
        label_mat = np.array(labellist)
        ids_str = ','.join(idlist)
        return vec_mat, label_mat, ids_str, epoch, offset


def get_train_data_loader(csvfile, datafile, num_class=203094, batch_size=5000, max_per_cat=30, gpu_num=1, num_workers=0):
    """
    :param csvfile: 训练集的csv文件路径
    :param datafile: 训练集的embedding文件路径，embedding文件由torch.save保存的一个pkl文件，内含一个dict对象，
                        key是图片的id，value是图片的embedding由numpy数组表示。
    :param num_class: 类别数量
    :param batch_size: 每次组出的batch当中的样本数量。
    :param max_per_cat: 组batch时每个类别的最大样本数
    :param gpu_num: 每次会输出一个 gpu_num * batch_size * dim 的tensor。为了让多GPU时每个GPU都能得到一个组出的batch进行训练。
    :param num_workers:
    :return:
    """
    dataset = GDataset(csvfile, datafile, num_class=num_class, batch_size=batch_size, max_per_cat=max_per_cat)
    return DataLoader(dataset, batch_size=gpu_num, shuffle=False, num_workers=num_workers, pin_memory=True)


if __name__ == '__main__':
    dataloader = get_train_data_loader('/data/dataset/google-landmark/train.csv',
                                       '/data/yushang/landmark/pair/output/net_embed_train.pkl',
                                       num_class=203094, batch_size=5000, max_per_cat=30,
                                       gpu_num=8, num_workers=8)
    ts = time.time()
    for i, (vectors, labels, ids_str, epochs, offsets) in enumerate(dataloader):
        # print(i)
        # print(vectors.shape)
        # print(labels)
        # print(ids_str)
        # print(epochs)
        # print(offsets)
        if (i + 1) % 10 == 0:
            speed = '%.2f' % (labels.shape[0] * 10.0 / (time.time() - ts))
            ts = time.time()
            print("step:", i, 'batch/sec:', speed, 'epoch: ', epochs.numpy()[0])

    """
    :returns:
    vectors:  gpu_num * batch_size * dim 的tensor
    labels: gpu_num * batch_size 的tensor
    ids_str: gpu_num长度的list，每个元素是一个str，这个str由batch_size个样本的id加逗号分隔。
    epochs: gpu_num 大小的tensor，每个元素为那个batch所处的epoch。
    offsets: gpu_num 大小的tensor，每个元素为那个batch组装结束时offset的位置，由于worker是异步的，所以offset只能当作进度的参考。
    """


